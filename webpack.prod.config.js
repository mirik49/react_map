const webpack = require('webpack');
const merge = require('webpack-merge');
const common = require('./common.config');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
module.exports = merge(common, {
    stats: {
        colors: false,
        hash: true,
        timings: true,
        assets: true,
        chunks: true,
        chunkModules: true,
        modules: true,
        children: true,
    },
    optimization: {
        minimizer: [
            new UglifyJsPlugin({
                sourceMap: true,
                uglifyOptions: {
                    compress: {
                        inline: false,
                    },
                },
            }),
        ],
        runtimeChunk: false,
        splitChunks: {
            cacheGroups: {
                default: false,
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendor_app',
                    chunks: 'all',
                    minChunks: 2,
                },
            },
        },
    },
});
