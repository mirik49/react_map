const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const WriteFilePlugin = require('write-file-webpack-plugin');

module.exports = {
    entry:
        {
            'main.css': './src/scss/main.scss',
            'main.js': './src/js/main.tsx',
        },
    output: {
        path: path.join(__dirname, "public/assets"),
        filename: '[name]'
    },

    module: {
        rules: [
            {test: /\.(js|jsx|ts|tsx)$/, exclude: /node_modules/, loader: 'ts-loader'},
            {
                test: /\.(png|jpg|jpeg|webp|svg)$/i,
                loader: 'file-loader',
                options: {
                    name: 'image/[name].[ext]',
                },
            },
            {
                test: /\.(woff|woff2)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: 'fonts/[name].[ext]'
                    }
                }]
            },

            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'sass-loader']
                })
            },
        ]
    },
    plugins: [
        new ExtractTextPlugin({filename: 'main.css'}),
        new HtmlWebpackPlugin({template: "index.html"}),
        new WriteFilePlugin(),
        new CopyPlugin([
            {from: path.resolve(__dirname, 'src/img/'), to: path.resolve(__dirname, 'public/assets/img')},
        ])
    ],
    resolve: {
        extensions: ['.js', '.ts', '.jsx', '.tsx']
    }
};
