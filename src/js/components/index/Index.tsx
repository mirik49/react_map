import React, {useEffect, useState} from "react";
import {Map, View, Collection} from "ol";
import TileLayer from "ol/layer/Tile";
import OSM from "ol/source/OSM";
import Point from "ol/geom/Point";
import {MousePosition, ScaleLine} from "ol/control";
import Overlay from "ol/Overlay";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import {LineString, Polygon} from "ol/geom";
import {getArea, getLength} from "ol/sphere";
import {Circle as CircleStyle, Fill, Stroke, Style, Icon} from "ol/style";
import Draw from "ol/interaction/Draw";
import {unByKey} from "ol/Observable";
import Feature from "ol/Feature";
import {Form, Card, Button} from "antd";

export default function Index() {

  const basePointList = [
    {
      id: 1,
      title: "title1",
      description: "description1",
      coordinates: [4420473.128288177, 5981906.298371299]
    },
    {
      id: 2,
      title: "title2",
      description: "description2",
      coordinates: [4420290.396017432, 5981647.129072269]
    },
    {
      id: 3,
      title: "title3",
      description: "description3",
      coordinates: [4420702.439373032, 5981968.403456781]
    }
  ];

  const [renderModal, setRenderModal] = useState({render: false, coordinate: []});
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [coordinates, setCoordinates] = useState("");
  const [pointList, setPointList] = useState(basePointList);
  const [features, setFeatures] = useState([]);
  const [renderPointInfo, setRenderPointInfo] = useState({show: false, feature: {}});
  let measureTooltipElement;
  let measureTooltip;

  useEffect(() => {

    const container = document.getElementById("popup");
    const overlay = new Overlay({
      element: container,
      autoPan: true,
      autoPanAnimation: {
        duration: 250
      }
    });

    const map = new Map({
      target: "map",
      views: new VectorLayer({}),
      layers: [
        //@ts-ignore
        new TileLayer({
          //@ts-ignore
          source: new OSM()
        })
      ],
      overlays: [overlay]
    });
    let sketch;
    let helpTooltipElement;// = document.getElementById("toolTip");
    const continueLineMsg = "Click to continue drawing the line";
    const continuePolygonMsg = "Click to continue drawing the polygon";
    let helpTooltip;
    const typeSelect = document.getElementById("type");
    let draw;
    const source = new VectorSource();

    const pointerMoveHandler = (evt) => {

      if (evt.dragging) {

        return;

      }
      /** @type {string} */
      let helpMsg = "Click to start drawing";

      if (sketch) {

        const geom = sketch.getGeometry();
        if (geom instanceof Polygon) {

          helpMsg = continuePolygonMsg;

        } else if (geom instanceof LineString) {

          helpMsg = continueLineMsg;

        }

      }

      helpTooltipElement.innerHTML = helpMsg;
      helpTooltip.setPosition(evt.coordinate);

      helpTooltipElement.classList.remove("hidden");

    };
    const formatLength = function (line) {

      const length = getLength(line);
      let output;
      if (length > 100) {

        output = (Math.round(length / 1000 * 100) / 100) +
                    " " + "km";

      } else {

        output = (Math.round(length * 100) / 100) +
                    " " + "m";

      }
      return output;

    };

    const formatArea = function (polygon) {

      const area = getArea(polygon);
      let output;
      if (area > 10000) {

        output = (Math.round(area / 1000000 * 100) / 100) +
                    " " + "km<sup>2</sup>";

      } else {

        output = (Math.round(area * 100) / 100) +
                    " " + "m<sup>2</sup>";

      }
      return output;

    };


    function addInteraction() {

      //@ts-ignore
      const type = (typeSelect.value === "area" ? "Polygon" : "LineString");
      draw = new Draw({
        //@ts-ignore
        source: source,
        //@ts-ignore
        type: type,
        style: new Style({
          fill: new Fill({
            color: "rgba(255, 255, 255, 0.2)"
          }),
          stroke: new Stroke({
            color: "rgba(0, 0, 0, 0.5)",
            lineDash: [10, 10],
            width: 2
          }),
          //@ts-ignore

          image: new CircleStyle({
            radius: 5,
            stroke: new Stroke({
              color: "rgba(0, 0, 0, 0.7)"
            }),
            fill: new Fill({
              color: "rgba(255, 255, 255, 0.2)"
            })
          })
        })
      });
      map.addInteraction(draw);

      let listener;
      draw.on("drawstart",
        function (evt) {

          // set sketch
          sketch = evt.feature;

          let tooltipCoord = evt.coordinate;

          listener = sketch.getGeometry().on("change", function (evt) {

            const geom = evt.target;
            let output;
            if (geom instanceof Polygon) {

              output = formatArea(geom);
              //@ts-ignore
              tooltipCoord = geom.getInteriorPoint().getCoordinates();

            } else if (geom instanceof LineString) {

              output = formatLength(geom);
              //@ts-ignore
              tooltipCoord = geom.getLastCoordinate();

            }
            measureTooltipElement.innerHTML = output;
            measureTooltip.setPosition(tooltipCoord);

          });

        });

      draw.on("drawend",
        function () {

          measureTooltipElement.className = "ol-tooltip ol-tooltip-static";
          measureTooltip.setOffset([0, -7]);
          // unset sketch
          sketch = null;
          // unset tooltip so that a new one can be created
          measureTooltipElement = null;
          createMeasureTooltip();
          unByKey(listener);

        });

    }

    function createMeasureTooltip() {

      if (measureTooltipElement) {

        measureTooltipElement.parentNode.removeChild(measureTooltipElement);

      }
      measureTooltipElement = document.createElement("div");
      measureTooltipElement.className = "ol-tooltip ol-tooltip-measure";
      measureTooltip = new Overlay({
        element: measureTooltipElement,
        offset: [0, -15],
        //@ts-ignore
        positioning: "bottom-center"
      });
      map.addOverlay(measureTooltip);

    }

    function createHelpTooltip() {

      if (helpTooltipElement) {

        helpTooltipElement.parentNode.removeChild(helpTooltipElement);

      }
      helpTooltipElement = document.createElement("div");
      helpTooltipElement.className = "ol-tooltip hidden";
      helpTooltip = new Overlay({
        element: helpTooltipElement,
        offset: [15, 0],
        //@ts-ignore
        positioning: "center-left"
      });
      map.addOverlay(helpTooltip);

    }

    typeSelect.onchange = function () {

      map.removeInteraction(draw);
      addInteraction();

    };

    //вызов модалки добавления точки
    map.on("dblclick", function (evt) {

      const coordinate = evt.coordinate;
      setRenderPointInfo({show: false, feature: {}});
      setRenderModal({render: true, coordinate: coordinate});
      setCoordinates(coordinate);
      overlay.setPosition(coordinate);

    });

    const iconStyle = new Style({
      //@ts-ignore
      image: new Icon({
        anchor: [0.5, 0.96],
        anchorXUnits: "fraction",
        anchorYUnits: "pixels",
        src: "../../../img/town3.png"
      })
    });

    pointList.map((element) => {

      //@ts-ignore
      const feature = new Feature({
        //  @ts-ignore
        geometry: new Point(element.coordinates),
        title: element.title,
        description: element.description
      });
      //  @ts-ignore
      feature.setStyle(iconStyle);

      features.push(
        feature
      );

    });
    setFeatures(features);

    //@ts-ignore
    const collections = new Collection(features);

    const vectorLayer = new VectorLayer({
      source: new VectorSource({
        //@ts-ignore
        features: collections
      })
    });

    map.addLayer(vectorLayer);
    map.addControl(new MousePosition());
    map.addControl(new ScaleLine());
    map.setView(new View({
      center: [4420562.702930699, 5981642.351758002],
      zoom: 17
    })
    );

    //измерения
    // addInteraction();

    createHelpTooltip();
    createMeasureTooltip();
    map.on("pointermove", (evt) => pointerMoveHandler(evt));
    map.getViewport().addEventListener("mouseout", function () {

      helpTooltipElement.classList.add("hidden");

    });

    map.on("click", function (evt) {

      map.forEachFeatureAtPixel(evt.pixel, function (feature, layer) {

        setRenderModal({render: false, coordinate: []});
        const coordinate = evt.coordinate;
        overlay.setPosition(coordinate);
        setRenderPointInfo({show: !renderPointInfo.show, feature: feature});
        setTimeout(() => {

          setRenderPointInfo({show: false, feature: {}});

        }, 1000);

      });

    });

  }, []);


  const addPointToLost = () => {

    //@ts-ignore
    // const newFeature = new Feature({
    //   //  @ts-ignore
    //   geometry: new Point(coordinates),
    //   title: title,
    //   description: description
    // });

    const point = {
      title,
      description,
      coordinates
    };
    //@ts-ignore
    // collections.push(newFeature);
    pointList.push(point);
    setPointList(pointList);
    setRenderModal({render: false, coordinate: []});

  };

  const renderPointInfoPopup = () => {

    //@ts-ignore
    const value = renderPointInfo.feature.hasOwnProperty("values_") ? renderPointInfo.feature.values_ :
    //@ts-ignore
      renderPointInfo.feature.hasOwnProperty("element") ? renderPointInfo.feature.element : "" ;

    return (
      <Card title={value.title} style={{width: "170px", padding: "5px"}}>
        <h2>{}</h2>
        <p>{value.description}</p>
      </Card>
    );

  };

  const renderModalContent = () => (
    <Form style={{display: "flex", flexDirection: "column"}}>

      <input type={"primary"} onChange={(event) => {

        setTitle(event.target.value);

      }} value={title}
      placeholder="title"/>

      <input
        onChange={((event) => {

          setDescription(event.target.value);

        })} value={description}
        placeholder={"description"}/>
      <Button onClick={() => addPointToLost()}>Save</Button>
    </Form>
  );

  const renderActualPlaces = () => (
    <div style={{position: "relative", zIndex: "0"}} id="map">
      <div style={{position: "absolute", top: "60px", right: "20px", zIndex: "22"}}>
        {
          pointList.map((element, key) => (
            <Card title={element.title} key={key} data-coordinates={element.coordinates}
              bordered={false} style={{width: 150, marginBottom: "20px"}}
              onClick={() => {

                setRenderPointInfo({show: true, feature: {element}});
                setRenderModal({render: false, coordinate: []});

              }}>
              <p>{element.description}</p>
            </Card>
          ))}
      </div>
    </div>
  );

  return (
    <section>
      <form className="form-inline">
        <select id="type">
          <option value="length">Length (LineString)</option>
          <option value="area">Area (Polygon)</option>
        </select>
      </form>
      {renderActualPlaces()}
      <div id="popup">
        {renderModal.render ? renderModalContent() : null}
        {renderPointInfo.show ? renderPointInfoPopup() : null}
      </div>
    </section>
  );

}
