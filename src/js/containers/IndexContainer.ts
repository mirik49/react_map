import {connect} from "react-redux";
import Index from "../components/index/Index";
import {setActionData} from "../store/index/actions";

const mapDispatchToProps = {
  setActionData
};

export default connect(state=>state, mapDispatchToProps)(Index);
