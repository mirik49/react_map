import * as React from "react";
import {Router} from "react-router";
import "../scss/main.scss";
import IndexContainer from "./containers/IndexContainer.ts";
import {Provider} from "react-redux";
import {BrowserRouter} from "react-router-dom";
import {createBrowserHistory} from "history";

export const history = createBrowserHistory();
import combineReducers from "./store/reducers";
import {applyMiddleware, createStore} from "redux";
import {routerMiddleware} from "react-router-redux";
import {render} from "react-dom";
import thunk from "redux-thunk";

export const store = createStore(combineReducers,
  applyMiddleware(thunk, routerMiddleware(history)));

render(
  <Provider store={store}>
    <Router history={history}>
      <BrowserRouter>
        <IndexContainer/>
      </BrowserRouter>
    </Router>
  </Provider>,
  document.getElementById("app")
);
