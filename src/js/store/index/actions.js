export function setActionData(actionVar, actionData) {

  return {
    type: "ACTIONS",
    payload: {
      var: actionVar,
      val: actionData
    }
  };

}
