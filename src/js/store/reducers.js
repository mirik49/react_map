import {combineReducers} from "redux";

import {indexReducer} from "./index/reducers";
import React from "react";

export const baseState = {
  date: new Date()
};

export const baseReducer = (state = baseState, action) => {

  switch (action.type) {

  }
  return state;

};

export default combineReducers({
  base: baseReducer,
  index: indexReducer

});


